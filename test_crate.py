from draw_maze import parse_grid
from moves import move
from moves import LEFT, RIGHT, UP, DOWN
import pytest

LEVEL = """#######
#.....#
#..o..#
#.o*o.#
#..o..#
#.....#
#######"""

PATHS = [
    (UP, LEFT),
    (LEFT, UP),
    (RIGHT, UP, LEFT, LEFT),
    pytest.mark.xfail((DOWN, DOWN))     # spodziewane niepowodzenie
]

PATH_PLAYERPOS = [
    ((LEFT,), 2, 3),
    ((LEFT, RIGHT), 3, 3),
    ((RIGHT, RIGHT), 4, 3)
]

@pytest.fixture
def level():
    """Cztery pojedyncze sciany"""
    return parse_grid(LEVEL)


def test_move_crate_to_corner(level):
    # Przenoszenie gornej skrzynki do lewego gornego naroznika
    for d in [UP, RIGHT, UP, LEFT, LEFT, LEFT]:
        move(level, d)
    assert level[1][1] == 'o'


def test_move_crate_back_forth(level):
    # Test zdrowego rozsadku: przesuwanie gornej skrzyni dwukrotnie
    for d in [LEFT, UP, RIGHT, UP, RIGHT, RIGHT, DOWN, LEFT, LEFT, LEFT]:
        move(level, d)
    assert level[2] == list('#o*   #')


@pytest.mark.parametrize('path', PATHS)
def test_paths(path, level):
    # rozne sciezki prowadzace do tego samego miejsca
    for direction in path:
        move(level, direction)
    assert level[2][2] == '*'


@pytest.mark.parametrize('path, expected_x, expected_y', PATH_PLAYERPOS)
def test_move_player(level, path):
    # polozenie pozycji gracza zmienilo sie prawidlowo
    for direction in path:
        move(level, direction)
    assert level[expected_y][expected_x] == '*'
