# Code for chapter 02 - Exceptions in Python

from pygame import image, Rect, Surface


TILE_POSITIONS = [
    ('#', 0, 0),    # wall
    ('o', 1, 0),    # crate
    (' ', 0, 1),    # floor
    ('x', 1, 1),    # exit
    ('.', 2, 0),    # dot
    ('*', 3, 0),    # player
]

SIZE = 32


def get_tile_rect(x, y):
    """Converts tile indices to a pygame.Rect"""
    return Rect(x * SIZE, y * SIZE, SIZE, SIZE)


def load_tiles():
    """
    Load tiles from an image file into a dictionary.
    Returns a tuple of (image, tile_dict)
    """
    tiles = {}
    tile_img = image.load('tiles.xpm')
    for sign, x, y in TILE_POSITIONS:
        tiles[sign] = get_tile_rect(x, y)
    return tile_img, tiles


if __name__ == '__main__':
    tile_img, tiles = load_tiles()
    # Surface representing image
    m = Surface((96, 32))
    # blit draw many images onto another
    m.blit(tile_img, get_tile_rect(0, 0), tiles['#'])
    m.blit(tile_img, get_tile_rect(1, 0), tiles[' '])
    m.blit(tile_img, get_tile_rect(2, 0), tiles['*'])
    image.save(m, 'tile_combo.png')


